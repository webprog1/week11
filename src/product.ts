import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new product into the database...")
    const productsRepisitory = AppDataSource.getRepository(Product);
    
    const Products = await productsRepisitory.find()
    console.log("Loaded products: ", Products)

    const updateProduct = await productsRepisitory.findOneBy({id:1})
    console.log(updateProduct)
    updateProduct.price = 80
    await productsRepisitory.save(updateProduct)
}).catch(error => console.log(error))
